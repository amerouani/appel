public class User {
    private String username;
    private Integer streak;
    private Integer delayed;

    public User(String username) {
        this.username = username;
        this.streak = 0;
        this.delayed = 0;
    }

    public void addStreak(Integer streak) {
        this.streak += streak;
    }
    public void addDelayed(Integer Delayed) {
        this.delayed += Delayed;
    }
    public void resetStreak(){
        this.streak = 0;
    }
    public Integer getStreak(){

        return this.streak;
    }

    public String getUsername() {
        return username;
    }

    public Integer getDelayed() {
        return delayed;
    }
}
