import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;

public class IrcBridge implements Runnable {
    private final BufferedWriter output;
    private final BufferedReader input;

    public String channel;
    public String nick;
    Thread t;
    public String[] commandList;
    PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private String[] userInput;
    private ArrayList<String> users;

    public IrcBridge(String host, Integer port, String nick, String channel, String[] commandList) throws Exception {
        this.nick = nick;
        this.channel = channel;
        this.commandList = commandList;
        t = new Thread(this);
        SSLSocket fd = (SSLSocket) SSLSocketFactory.getDefault().createSocket(InetAddress.getByName(host), port);
        input = new BufferedReader(new InputStreamReader(fd.getInputStream()));
        output = new BufferedWriter(new OutputStreamWriter(fd.getOutputStream()));
    }

    public void connect() {
        try {

            String recv;

            output.write("NICK " + this.nick + "\r\n");
            output.write("USER " + this.nick + " " + this.nick + ": Java bot\r\n");
            output.flush();

            while ((recv = input.readLine()) != null) {
                System.out.println(recv);
                if (recv.contains("You are connected")) {
                    this.sendCmd("JOIN " + this.channel);
                    break;
                }
            }
            while((recv = input.readLine()) != null){
                if (recv.contains("353")) {
                    String[] split = recv.split(":")[2].split(" ");
                    for (int i = 0; i < split.length; i++) {
                        if(split[i].equals(this.nick)){
                            continue;
                        }
                        StringBuilder mod = new StringBuilder(split[i]);
                        if (split[i].startsWith("~") || split[i].startsWith("@") || split[i].startsWith("%") || split[i].startsWith("+")) {
                            split[i] = mod.deleteCharAt(0).toString();
                        }
                    }
                    this.setUsers(split);
                    break;
                }
            }
            t.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        String recv;
        try {
            while (true) {
                while ((recv = input.readLine()) != null) {

                    System.out.println(recv);

                    if (recv.startsWith("PING")) {
                        this.sendCmd("PONG " + ":" + recv.split(":")[1]);
                    }

                    if (recv.contains(this.channel.toLowerCase() + " :.")) {
                        String split =  recv.split(":")[2];

                        String user = recv.split(":")[1].split("!")[0];

                        String command = split.split(" ")[0];

                        for (String s : commandList) {
                            if(s.equals(command)){
                                addUserInput(user, split);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendCmd(String command) throws Exception {
        output.write(command + "\r\n");
        output.flush();
    }

    public void sendMsg(String message) throws Exception {
        output.write("PRIVMSG " + this.channel + " " + message + " \r\n");
        output.flush();

    }

    public ArrayList<String> getUsers() {
        return users;
    }

    public void setUsers(String[] users_input) {
        this.users = new ArrayList<String>();
        for (String user : users_input) {
            this.users.add(user);
        }
    }

    /**
     * Obervaters
     **/

    public void addObserver(PropertyChangeListener l) {
        pcs.addPropertyChangeListener(l);
    }

    public void removeObserver(PropertyChangeListener l) {
        pcs.removePropertyChangeListener(l);
    }

    public void addUserInput(String user, String command) {
        String[] newvalue = new String[]{user, command};
        pcs.firePropertyChange("userCommand", null, newvalue);
    }
}
