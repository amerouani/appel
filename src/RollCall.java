import java.beans.PropertyChangeEvent;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class RollCall {

    private HashMap<String,User> registred;
    private HashMap<String,User> present;
    private HashMap<String, User> latecomer;
    private Boolean inCall = false;
    public IrcBridge irc;
    public String[] commandList = {".join", ".help", ".present", ".info", ".leave", ".delay"};

    public static void main (String[] args) throws Exception {
        RollCall roll = new RollCall();
        Timer timer = new Timer();

        Calendar callDate = Calendar.getInstance();
        Calendar endCallDate = Calendar.getInstance();

        callDate.set(Calendar.HOUR_OF_DAY, 9);
        callDate.set(Calendar.MINUTE, 00);

        endCallDate.set(Calendar.HOUR_OF_DAY, 9);
        endCallDate.set(Calendar.MINUTE, 45);
        timer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            roll.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, callDate.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
        timer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            roll.endCall();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, endCallDate.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
    }
    public void init() {
        this.registred = new HashMap<String, User>();
        this.present = new HashMap<String, User>();
        this.latecomer = new HashMap<String, User>();
        this.irc.addObserver(this::propertyChange);
        this.irc.connect();
    }
    public RollCall() throws Exception {
        this.irc = new IrcBridge("irc.xolus.net", 6697, "AppelMatinal","#n-pn", this.commandList);
        this.init();
    }

    private void call() throws Exception {
        this.inCall = true;
        List<String> keys = new ArrayList<>(registred.keySet());
        irc.sendMsg("== APPEL EN COURS ==");
        irc.sendMsg("Sont invité a répondre .present la liste d'utilisateurs: " + keys);
    }
    private void delay(String user) throws Exception {
        if(!this.inCall){
            Calendar delayLimitHigh = Calendar.getInstance();
            Calendar delayLimitLow = Calendar.getInstance();

            Calendar now = Calendar.getInstance();

            delayLimitHigh.set(Calendar.HOUR_OF_DAY, 10);
            delayLimitHigh.set(Calendar.MINUTE, 20);

            delayLimitLow.set(Calendar.HOUR_OF_DAY, 9);
            delayLimitLow.set(Calendar.MINUTE, 46);

            if(now.before(delayLimitHigh) && now.after(delayLimitLow)){
                if(this.latecomer.containsKey(user)){
                    this.registred.put(user, this.latecomer.get(user));
                    this.registred.get(user).addStreak(1);
                    this.registred.get(user).addDelayed(1);
                    this.latecomer.remove(user);
                    irc.sendMsg("Retard excusé ! Vous êtes inscrit au prochain appel.");
                } else {
                    irc.sendMsg("/!\\ Vous ne faites pas partie des retardataires, vous n'étiez sûrement pas inscrit a l'appel courant.");
                }
            } else {
                irc.sendMsg("/!\\ Billet de retard non recevable, 35 minutes ont passé depuis l'appel ou l'appel n'a pas encore eu lieu.");
            }
        } else {
            irc.sendMsg("/!\\ Billet de retard non recevable, appel en cours.");
        }
    }
    private void endCall() throws Exception {
        List<String> keys = new ArrayList<>(present.keySet());
        this.latecomer.putAll(this.registred);
        this.registred.clear();
        this.registred.putAll(this.present);
        irc.sendMsg("ont répondu présent, les utilisateurs (et sont donc automatiquement inscrit au prochain appel): " + keys);
        irc.sendMsg("== APPEL FINIT ==");
        this.present.clear();
        this.inCall = false;
    }
    private void help() throws Exception{
        irc.sendMsg("Un appel tout les jours à partir de 9h jusqu'à 9h45, il vous faut vous enregistrer d'abord a l'appel via la commande .join, puis lors de l'appel répondre présent via la commande .present.");
        irc.sendMsg("Si vous avez loupé un appel, il est possible de fournir un billet de retard jusqu'à 35 min après la fin de l'appel avec .delay");
        irc.sendMsg("Si vous vous sentez pas la force de relever le défi quotidien que vous vous êtes imposé vaillemment, vous pouvez quitté l'appel via la commande .leave");
    }
    private void leave(String user) throws Exception{
        if(registred.containsKey(user)){
            registred.remove(user);
            irc.sendMsg("Tu as été enlevé de l'appel.");
        } else{
            irc.sendMsg("/!\\ Tu ne peux pas quitter un appel que tu n'as pas rejoint. ¯\\_(ツ)_/¯");
        }
    }
    private void info(String user) throws Exception{
        if(registred.containsKey(user)){
            irc.sendMsg("Tu es enregistré pour le prochain appel de 9h, tu as répondu présent " + registred.get(user).getStreak() + " fois, tu as été en retard "+ registred.get(user).getDelayed() +" fois. Ce qui est assez énorme au final.");
        }
        else{
            irc.sendMsg("/!\\ Tu n'es pas enregistré dans l'appel, je t'invite a le faire avec la commande .join.");
        }
    }
    private void present(String user) throws Exception {
        if(this.inCall){
            if(this.registred.containsKey(user)){
                if(!this.present.containsKey(user)){
                    this.present.put(user, new User(user));
                    this.present.get(user).addStreak(1);
                    this.registred.remove(user);
                    irc.sendMsg("Merci de ta présence. Tu as été présent " + this.present.get(user).getStreak() + " fois d'affilées, gg. « Les petits actes de discipline réalisés chaque jour nous mènent éventuellement à de grandes réalisations. ~ John C. Maxwell »");
                } else {
                    irc.sendMsg("/!\\ Tu as déjà répondu présent.");
                }
            } else {
                irc.sendMsg("/!\\ Vous n'êtes pas enregistré dans l'appel actuel, enregistrez vous via la commande .join, lorsque cet appel sera finit.");
            }
        } else{
            irc.sendMsg("/!\\ Vous ne devez répondre présent uniquement lorsqu'un appel est en cours. ");
        }
    }
    private void join(String user) throws Exception {
        if(!this.inCall){
            if(!this.registred.containsKey(user)){
                this.registred.put(user, new User(user));
                irc.sendMsg("Tu es enregistré pour le prochain appel");
            }
            else{
            irc.sendMsg("/!\\ Tu es déjà enregistré pour le prochain appel.");
            }
        }
        else
            irc.sendMsg("/!\\ Un appel est en cours, tu ne peux pas t'enregistrer actuellement, attends que l'appel en cours soit finit.");
    }

    public void propertyChange(PropertyChangeEvent evt){
        try {
            String[] input = (String[]) evt.getNewValue();

            String username = input[0];
            String command = input[1].substring(1).split(" ")[0];

            if(command.equals("join")){
                this.join(username);
            }
            else if(command.equals("present")){
                this.present(username);
            }
            else if(command.equals("help")){
                this.help();
            }
            else if(command.equals("info")){
                this.info(username);
            }
            else if(command.equals("leave")){
                this.leave(username);
            }
            else if(command.equals("delay")){
                this.delay(username);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
